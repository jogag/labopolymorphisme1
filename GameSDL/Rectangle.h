#pragma once
#include "GameObject.h"

class Quadrilatere : public GameObject
{
private:
	int speed;
	int directionX;

public:
	void Draw(SDL_Renderer* graphics);
	void Update(int windowWidth, int windowHeight);
	Quadrilatere();
	Quadrilatere(int x, int y, int width, int height, int r, int g, int b, int speed, int dirX);
	~Quadrilatere();
};