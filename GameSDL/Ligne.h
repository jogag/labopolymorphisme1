#pragma once
#include "GameObject.h"

class Ligne : public GameObject
{
private:
	int x2;
	int y2;

public:
	void Draw(SDL_Renderer* graphics);
	void Update();
	Ligne();
	Ligne(int x, int y, int x2, int y2, int r, int g, int b);
	~Ligne();
};