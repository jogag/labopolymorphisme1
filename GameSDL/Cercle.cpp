#include "Cercle.h"

void Cercle::Draw(SDL_Renderer* graphics)
{
	double initRayon = rayon;
	double angle = 0.0f;

	while (angle != 360)
	{
		SDL_SetRenderDrawColor(graphics, m_r, m_g, m_b, 255);
		SDL_RenderDrawPoint(graphics, x + (initRayon * (cos(angle))), y + (initRayon * (sin(angle))));
		angle++;
	}
}

void Cercle::Update(int windowWidth, int windowHeight)
{
	y += speed;
	if (y - rayon <= 0 || y + rayon >= windowHeight)
	{
		speed *= -1;
	}
}

Cercle::Cercle() : Cercle(0, 0, 0, 0, 1, 0, 0, 0)
{
	SDL_Log("Constructeur par defaut de Cercle");
}

Cercle::Cercle(int x, int y, float rayon, int speed, int dirY, int r, int g, int b) : rayon(rayon), speed(speed), directionY(dirY)
{
	this->x = x;
	this->y = y;
	this->m_r = r;
	this->m_g = g;
	this->m_b = b;

	SDL_Log("Constructeur par param de Cercle");
}

Cercle::~Cercle()
{
	SDL_Log("Destructeur de Cercle");
}
