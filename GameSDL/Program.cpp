#include <windows.h>
#include <SDL.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "Games.h"

int WINAPI WinMain(HINSTANCE h, HINSTANCE i, LPSTR c, int n)
{
	Games Lab;
	if (Lab.Init("Polymorphisme", 800, 800))
	{
		Lab.Start();
	}
	return 0;
}