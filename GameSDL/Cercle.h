#include "GameObject.h"
#pragma once

class Cercle : public GameObject
{
private:
	int speed;
	int directionY;
	float rayon;

public:
	void Draw(SDL_Renderer* graphics);
	void Update(int windowWidth, int windowHeight);
	Cercle();
	Cercle(int x, int y, float rayon, int speed, int dirY, int r, int g, int b);
	~Cercle();
};
