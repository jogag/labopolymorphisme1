#include <iostream>
#include "Ligne.h"

void Ligne::Draw(SDL_Renderer* graphics)
{
	SDL_SetRenderDrawColor(graphics, m_r, m_g, m_b, 255);
	SDL_RenderDrawLine(graphics, x, y, x2, y2);
	
}

void Ligne::Update()
{
}

Ligne::Ligne() : Ligne(0, 0, 0, 0, 0, 0, 0)
{
	SDL_Log("Constructeur par defaut de Ligne");
}

Ligne::Ligne(int x, int y,int x2, int y2 , int r, int g, int b)
{
	this->x = x;
	this->y = y;
	this->x2 = x2;
	this->y2 = y2;
	this->m_r = r;
	this->m_g = g;
	this->m_b = b;

	SDL_Log("Constructeur par param de Ligne");
}

Ligne::~Ligne()
{
	SDL_Log("Destructeur de Ligne");
}
