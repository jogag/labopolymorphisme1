#include "Rectangle.h"

void Quadrilatere::Draw(SDL_Renderer* graphics)
{
	SDL_SetRenderDrawColor(graphics, m_r, m_g, m_b, 255);
	SDL_Rect rect = { x, y, width, height };
	SDL_RenderFillRect(graphics, &rect);
}

void Quadrilatere::Update(int windowWidth, int windowHeight)
{	
	x += speed;
	if (x <= 0 || x + width >= windowWidth)
	{
		speed *= -1;
	}
}

Quadrilatere::Quadrilatere() : Quadrilatere(0, 0, 0, 0, 0, 0, 0, 0, 1)
{
	SDL_Log("Constructeur par defaut de Quadrilatere");
}

Quadrilatere::Quadrilatere(int x , int y, int width, int height, int r, int g, int b, int speed, int dirX) : speed(speed), directionX(dirX)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->m_r = r;
	this->m_g = g;
	this->m_b = b;

	SDL_Log("Constructeur par param de Quadrilatere");
}

Quadrilatere::~Quadrilatere()
{
	SDL_Log("Destructeur de Quadrilatere");
}
