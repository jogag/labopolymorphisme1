#include "GameObject.h"

GameObject::GameObject() : GameObject(0, 0, 0, 0, 0, 0, 0)
{
	SDL_Log("Constructeur par defaut de GameObject");

}

GameObject::GameObject(int x, int y, int width, int height, int r, int g, int b) : x(x), y(y), width(width), height(height), m_r(r), m_g(g), m_b(b)
{
	SDL_Log("Constructeur avec param de GameObject");

}

GameObject::~GameObject()
{
	SDL_Log("Destructeur de GameObject");
}

void GameObject::Draw(SDL_Renderer* graphics)
{
	SDL_SetRenderDrawColor(graphics, m_r, m_g, m_b, 255);
	SDL_Rect rect = { x, y , width, height };
	SDL_RenderFillRect(graphics, &rect);
}

void GameObject::SetPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}

void GameObject::SetSize(int width, int height)
{
	this->width = width;
	this->height = height;
}

void GameObject::Color(int r, int g, int b)
{
	m_r = r;
	m_g = g;
	m_b = b;
}

void GameObject::Update(int windowWidth, int windowHeight)
{
}
