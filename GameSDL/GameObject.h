#pragma once
#include <SDL.h>
#include <math.h>
#include <time.h>

class GameObject
{
protected:
	int x;
	int y;
	int width;
	int height;
	int m_r;
	int m_g;
	int m_b;

public:
	GameObject();
	GameObject(int x, int y, int width, int height, int r, int g, int b);
	virtual ~GameObject();
	virtual void Draw(SDL_Renderer* graphics);
	void SetPosition(int x, int y);
	void SetSize(int width, int height);
	void Color(int r, int g, int b);
	virtual void Update(int windowWidth, int windowHeight);
};