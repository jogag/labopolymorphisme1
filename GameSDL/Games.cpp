#include "Games.h"
#include "Rectangle.h"
#include "Ligne.h"
#include "Cercle.h"
#include <SDL.h>
#include <time.h>
#include <Windows.h>

bool Games::Init(const std::string& title, int width, int height)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		return false;
	}
	
	if (CreateGameWindow(title, width, height))
	{
		if (CreateRenderer())
		{
			m_Running = true;
			return true;
		}
		return false;
	}
	return false;
}

void Games::Start()
{
	const unsigned int FPS = 60;
	const unsigned int FRAME_TARGET_TIME = 1000 / FPS;

	InitialLoad();

	while (m_Running)
	{
		const clock_t start = clock();

		RenderFrame();
		UpdateLogic();
		ProcessInput();

		const clock_t end = clock();
		int rest = start + FRAME_TARGET_TIME - end;
		if (rest > 0)
		{
			Sleep(rest);
		}
	}

	Close();
}

bool Games::CreateGameWindow(const std::string& title, int width, int height)
{
	m_Window = SDL_CreateWindow(
		"Labo Polymorphisme",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width, height,
		SDL_WINDOW_UTILITY
	);

	if (m_Window == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		return false;
	}

	return true;
}

bool Games::CreateRenderer()
{
	m_Graphics = SDL_CreateRenderer(
		m_Window,
		0,
		SDL_RENDERER_ACCELERATED
	);

	if (m_Window == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		return false;
	}
	return true;
}

void Games::Close()
{
	SDL_DestroyRenderer(m_Graphics);
	SDL_DestroyWindow(m_Window);
	SDL_Quit();
	for (GameObject* g : m_GameObject)
	{
		delete(g);
	}
}

void Games::ProcessInput()
{
	SDL_Event events;
	SDL_PollEvent(&events);

	switch (events.type)
	{
	case SDL_QUIT:
		m_Running = false;
		break;
	}
	
}

void Games::UpdateLogic()
{
	//Game Logic
	UpdateObject();
}

void Games::RenderFrame()
{
	SDL_SetRenderDrawColor(m_Graphics, 0, 100, 100, 255);
	SDL_RenderClear(m_Graphics);

	DrawObjects();

	SDL_RenderPresent(m_Graphics);
}

void Games::InitialLoad()
{	
	AddGameObject(new Quadrilatere(100, 200, 100, 200, 0, 0, 255, 10, 1));
	AddGameObject(new Ligne(400, 400, 500, 560, 0, 255, 0));
	AddGameObject(new Cercle(600, 600, 100, 7, 1, 255, 0, 0));
}

void Games::DrawObjects()
{
	for (GameObject* g : m_GameObject)
	{
		g->Draw(m_Graphics);
	}
}

void Games::AddGameObject(GameObject* g)
{
	m_GameObject.push_back(g);
}

void Games::UpdateObject()
{
	for (GameObject* g : m_GameObject)
	{
		g->Update(800, 800);
	}
}
