#pragma once
#include <string>
#include <vector>
#include "GameObject.h"

using namespace std;

//Forward class declaration
struct SDL_Window;
struct SDL_Renderer;

class Games
{
public:
	bool Init(const std::string& title, int width, int height);
	void Start();

private: 
	bool CreateGameWindow(const std::string& title, int width, int height);
	bool CreateRenderer();
	void Close();

	void InitialLoad();
	void ProcessInput();
	void UpdateLogic();
	void RenderFrame();

	void DrawObjects();
	void AddGameObject(GameObject* g);
	void UpdateObject();

protected:
	vector<GameObject*> m_GameObject;

	SDL_Window* m_Window;
	SDL_Renderer* m_Graphics;
	bool m_Running = false;
	const unsigned char* m_Keyboard = nullptr;
};